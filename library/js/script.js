//=require tomes/cyberpunk.js
//=require tomes/fantasy.js

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

var app = {

    settings: {
        stage: document.querySelector('.your-quest'),
        reroll: document.querySelector('.reroll'),
        full_quest: [],
        counter: 0
    },

    init: function() {
        app.buildQuests();
        app.bindUIActions();
    },

    // Add event listeners
    bindUIActions: function() {
        app.settings.reroll.addEventListener('click', app.buildQuests);
    },

    // Build the quest part by part
    buildQuests: function() {
        // Wipe the slate clean
        app.settings.stage.innerHTML = '';
        app.settings.full_quest = [];
        app.settings.counter = 0;

        var tome = fantasy;

        // Set the color
        document.body.classList.add(tome.theme);

        // Roll the die
        app.settings.reroll.classList.add('roll-animation');

            // Build the quest parts
            app.settings.full_quest.push(app.get(tome.locations) + ' you see ' + app.get(tome.large_objects) + '.');

            app.settings.full_quest.push('It\'s ' + app.get(tome.time) + ' in the ' + app.get(tome.season) + '.<br/>' + app.get(tome.sky) + ', and ' + app.get(tome.weather) + '.');

            app.settings.full_quest.push('You see ' + app.get(tome.small_objects) + ', and hear ' + app.get(tome.hear) + '.');

            app.settings.full_quest.push('You have come here to ' + app.get(tome.quest) + ', and to retrieve ' + app.get(tome.item_state) + ' ' + app.get(tome.item_type) + ' known as ' + app.get(tome.item_adjective).capitalize() + ' ' + app.get(tome.item_noun).capitalize() + '.');

            app.settings.full_quest.push('You smell ' + app.get(tome.smell) + '.');

        app.displayQuests();
    },

    // Loop through all quest parts and display with a delay
    displayQuests: function() {
        setTimeout(function() {
            var div = document.createElement('div');
                div.innerHTML = app.settings.full_quest[app.settings.counter];
                app.settings.stage.appendChild(div);
                app.settings.counter++;

            if (app.settings.counter < app.settings.full_quest.length) {
                app.displayQuests();
            } else {
                app.settings.reroll.classList.remove('roll-animation');
            }
        }, 500);
    },

    // Gets a random item from the supplied array
    get: function(item) {
        return item[Math.floor(Math.random() * item.length)];
    }

};
app.init();
